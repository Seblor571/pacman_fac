package fr.seblor.pacman.screens;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.TimerTask;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import fr.seblor.pacman.TextureFactory;
import fr.seblor.pacman.entities.Dark;
import fr.seblor.pacman.entities.Ghost;
import fr.seblor.pacman.entities.PacMan;
import fr.seblor.pacman.entities.Pellet;
import fr.seblor.pacman.entities.Wall;
import fr.seblor.pacman.environment.Directions;
import fr.seblor.pacman.gui.Hud;
import fr.seblor.pacman.gui.Timer;
import fr.seblor.pacman.gui.WorldRenderer;

public class PlayingScreen implements Screen {

	private Hud hud;

	private SpriteBatch _batch;
	private WorldRenderer worldRenderer;

	private String _debugText = null;
	private BitmapFont _debugFont = new BitmapFont();

	/*
	 * Setting up the ticks
	 */
	private float tickParSec = 60f;
	private float delta = 0f;

	/*
	 * Getting the path lists
	 */
	private ArrayList<LinkedList<Vector2>> pathsList;

	public PlayingScreen() {
		super();

		hud = new Hud();

		/*
		 * Setting up the screen
		 */
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 1);

		/*
		 * Setting up the sprites
		 */
		_batch = new SpriteBatch();

		worldRenderer = new WorldRenderer();

		if (!worldRenderer.initialize("map1.maze"))
			_debugText = "Failed to initialize map.";

		// Timer for updating the paths
		java.util.Timer timer = new java.util.Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				updatePaths();
			}
		}, 0, 500);

		// Forcing resize to get the squareSize value
		worldRenderer.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		// Adding the elements to the HUD
		hud.addScoreElement(10, worldRenderer.getWorld().getHeight() * worldRenderer.getSquareSize() + 10);
		hud.addTimerElement(worldRenderer.getWorld().getWidth() * worldRenderer.getSquareSize() - Timer.getTimerWidth(),
				worldRenderer.getWorld().getHeight() * worldRenderer.getSquareSize() + 10);

	}

	/**
	 * This function update the paths of all ghosts to get the shortest path for
	 * reaching the PacMan
	 */
	private void updatePaths() {
		ArrayList<Vector2> ghostsPos = new ArrayList<Vector2>();
		for (Ghost ghost : worldRenderer.getWorld().getGhosts()) {
			ghostsPos.add(ghost.getPosition());
		}

		Vector2 correctedPacManPos = new Vector2();

		correctedPacManPos.x = (float) Math.floor(worldRenderer.getWorld().getPacman().getPosition().x);
		correctedPacManPos.y = (float) Math.floor(worldRenderer.getWorld().getPacman().getPosition().y);

		pathsList = worldRenderer.getWorld().getPathFinder().bestPaths(correctedPacManPos, ghostsPos);

		checkPacManLose();

		for (Ghost ghost : worldRenderer.getWorld().getGhosts()) {
			for (LinkedList<Vector2> path : pathsList) {
				if (path.getFirst().equals(ghost.getPosition())) {
					ghost.setPath(path);
				}
			}
		}

	}

	private void checkPacManLose() {
		for (Ghost ghost : worldRenderer.getWorld().getGhosts()) {
		}
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	private void listenKeys() {
		if (Gdx.input.isKeyJustPressed(Keys.Z) || Gdx.input.isKeyJustPressed(Keys.UP)) {
			worldRenderer.getWorld().getPacman().setNextAngle(Directions.UP);
		} else if (Gdx.input.isKeyJustPressed(Keys.Q) || Gdx.input.isKeyJustPressed(Keys.LEFT)) {
			worldRenderer.getWorld().getPacman().setNextAngle(Directions.LEFT);
		} else if (Gdx.input.isKeyJustPressed(Keys.S) || Gdx.input.isKeyJustPressed(Keys.DOWN)) {
			worldRenderer.getWorld().getPacman().setNextAngle(Directions.DOWN);
		} else if (Gdx.input.isKeyJustPressed(Keys.D) || Gdx.input.isKeyJustPressed(Keys.RIGHT)) {
			worldRenderer.getWorld().getPacman().setNextAngle(Directions.RIGHT);
		}
	}

	@Override
	public void render(float delta) {

		listenKeys();

		// Setting the background
		Gdx.gl.glClearColor(Color.LIGHT_GRAY.r, Color.LIGHT_GRAY.g, Color.LIGHT_GRAY.b, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

		_batch.setProjectionMatrix(worldRenderer.getCamera().combined);

		// Drawing
		_batch.begin();

		// Rendering the world
		worldRenderer.render(delta, _batch);

		// Tick-loop
		tick(delta);

		// Score
		hud.drawHUD(_batch);

		// Debug text ?
		if (_debugText != null)
			_debugFont.draw(_batch, _debugText, 10, Gdx.graphics.getHeight() - 10);
		_batch.end();
	}

	@Override
	public void resize(int width, int height) {
		worldRenderer.resize(width, height);
		// Replacing the elements to the HUD
		hud.setScorePosition(10, worldRenderer.getWorld().getHeight() * worldRenderer.getSquareSize() + 10);
		hud.setTimerPosition(
				worldRenderer.getWorld().getWidth() * worldRenderer.getSquareSize() - Timer.getTimerWidth(),
				worldRenderer.getWorld().getHeight() * worldRenderer.getSquareSize() + 10);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	/**
	 * Function called each 1/X second
	 * 
	 * @param delta
	 */
	public void tick(float delta) {

		this.delta += delta;
		if (this.delta > (1 / this.tickParSec)) {

			worldRenderer.getWorld().getGhosts().get(0)
					.setPosition(getNewPosGhostFlood(worldRenderer.getWorld().getGhosts().get(0)));
			worldRenderer.getWorld().getGhosts().get(1)
					.setPosition(getNewPosGhostSemiRandom(worldRenderer.getWorld().getGhosts().get(1)));
			worldRenderer.getWorld().getGhosts().get(2)
					.setPosition(getNewPosGhostRandom(worldRenderer.getWorld().getGhosts().get(2)));
			worldRenderer.getWorld().getGhosts().get(3)
					.setPosition(getNewPosGhostRandom(worldRenderer.getWorld().getGhosts().get(3)));

			worldRenderer.getWorld().getPacman().setPosition(getNewPosPacman());
			removePellet();

			this.delta = 0;
		}
	}

	private Vector2 getNewPosPacman() {
		float X = worldRenderer.getWorld().getPacman().getPosition().x;
		float Y = worldRenderer.getWorld().getPacman().getPosition().y;
		float newX = X, newY = Y;

		// If new angle is the opposite of the current one, update
		if (Directions.isSameWay(worldRenderer.getWorld().getPacman().getNextAngle(),
				worldRenderer.getWorld().getPacman().getAngle()))
			worldRenderer.getWorld().getPacman().updateAngle();

		// If PacMan is in a full block (maybe intersection ?)
		if (Math.floor(X) == (Math.floor(X * 100) / 100) && Math.floor(Y) == (Math.floor(Y * 100) / 100)) {

			// If PacMan can change direction
			if (checkNoWall(X, Y, worldRenderer.getWorld().getPacman().getNextAngle())) {
				worldRenderer.getWorld().getPacman().updateAngle();
			}
			// Else, if the PacMan cannot continue on the current direction.
			else if (!checkNoWall(X, Y, worldRenderer.getWorld().getPacman().getAngle())) {
				return worldRenderer.getWorld().getPacman().getPosition();
			}
		}

		switch (worldRenderer.getWorld().getPacman().getAngle()) {
		case Directions.UP:
			newY = Y - PacMan.speed;
			break;
		case Directions.DOWN:
			newY = Y + PacMan.speed;
			break;
		case Directions.LEFT:
			newX = X - PacMan.speed;
			break;
		case Directions.RIGHT:
			newX = X + PacMan.speed;
			break;
		}

		// Correcting the float values
		newX = (float) (Math.round(newX * 100)) / 100;
		newY = (float) (Math.round(newY * 100)) / 100;

		// Teleporting if PacMan out of the map
		if (newX < 0)
			newX = worldRenderer.getWorld().getWidth() - 1;
		else if (newX == worldRenderer.getWorld().getWidth() - 1)
			newX = 0;
		if (newY < 0)
			newY = worldRenderer.getWorld().getHeight() - 1;
		else if (newY == worldRenderer.getWorld().getHeight() - 1)
			newY = 0;

		TextureFactory.getInstance().setPacManTexture(worldRenderer.getWorld().getPacman().getAngle());
		updatePaths();
		return new Vector2(newX, newY);
	}

	private Vector2 getNewPosGhostSemiRandom(Ghost ghost) {
		return (ghost.isFollowingPath()) ? (getNewPosGhostFlood(ghost)) : (getNewPosGhostRandom(ghost));

	}

	private Vector2 getNewPosGhostRandom(Ghost ghost) {
		float X = ghost.getPosition().x;
		float Y = ghost.getPosition().y;
		float newX = X, newY = Y;

		// If Ghost is in a full block (maybe intersection ?)
		if (Math.floor(X) == (Math.floor(X * 10) / 10) && Math.floor(Y) == (Math.floor(Y * 10) / 10)) {
			int angle = ((int) Math.floor(Math.random() * 4)) * 90;
			// Ghost can't go back
			do {
				angle = ((int) Math.floor(Math.random() * 4)) * 90;
				// Get a new random direction if backward AND NOT in ghosts
				// house.
			} while (!checkNoWall(X, Y, angle) || (Directions.areOpposites(angle, ghost.getAngle()))
					&& !worldRenderer.getWorld().getGhostHouse().contains(ghost.getPosition()));
			ghost.setAngle(angle);
		}

		switch (ghost.getAngle()) {
		case Directions.UP:
			newY = Y - ghost.getSpeed();
			break;
		case Directions.DOWN:
			newY = Y + ghost.getSpeed();
			break;
		case Directions.LEFT:
			newX = X - ghost.getSpeed();
			break;
		case Directions.RIGHT:
			newX = X + ghost.getSpeed();
			break;
		}

		// Correcting the float values
		newX = (float) (Math.round(newX * 10)) / 10;
		newY = (float) (Math.round(newY * 10)) / 10;

		// Teleporting if Ghost out of the map
		if (newX < 0)
			newX = worldRenderer.getWorld().getWidth() - 1;
		else if (newX == worldRenderer.getWorld().getWidth() - 1)
			newX = 0;
		if (newY < 0)
			newY = worldRenderer.getWorld().getHeight() - 1;
		else if (newY == worldRenderer.getWorld().getHeight() - 1)
			newY = 0;

		return new Vector2(newX, newY);
	}

	private Vector2 getNewPosGhostFlood(Ghost ghost) {
		float X = ghost.getPosition().x;
		float Y = ghost.getPosition().y;
		float newX = X, newY = Y;

		// If Ghost is in a full block (maybe intersection ?)
		if (Math.floor(X) == (Math.floor(X * 10) / 10) && Math.floor(Y) == (Math.floor(Y * 10) / 10)) {
			ghost.setAngle(Directions.getDirFromCoords(ghost.getPosition(), ghost.getPath().get(1)));
			ghost.getPath().clear();
		}

		switch (ghost.getAngle()) {
		case Directions.UP:
			newY = Y - ghost.getSpeed();
			break;
		case Directions.DOWN:
			newY = Y + ghost.getSpeed();
			break;
		case Directions.LEFT:
			newX = X - ghost.getSpeed();
			break;
		case Directions.RIGHT:
			newX = X + ghost.getSpeed();
			break;
		}

		// Correcting the float values
		newX = (float) (Math.round(newX * 10)) / 10;
		newY = (float) (Math.round(newY * 10)) / 10;

		// Teleporting if Ghost out of the map
		if (newX < 0)
			newX = worldRenderer.getWorld().getWidth() - 1;
		else if (newX == worldRenderer.getWorld().getWidth() - 1)
			newX = 0;
		if (newY < 0)
			newY = worldRenderer.getWorld().getHeight() - 1;
		else if (newY == worldRenderer.getWorld().getHeight() - 1)
			newY = 0;

		return new Vector2(newX, newY);
	}

	/**
	 * Returns the possibility for the entity to move according to the provided
	 * coordinates & angle
	 * 
	 * @param currentX
	 * @param currentY
	 * @param angle
	 * @return
	 */
	private boolean checkNoWall(float currentX, float currentY, int angle) {
		int X = (int) Math.floor(currentX);
		int Y = (int) Math.floor(currentY);
		// System.out.println("X: " + X + " | Y: " + Y);

		/*
		 * For each direction case, checking for the map limits. If touching the
		 * map border, teleporting to next side of the map. Else, we check if
		 * the next GameElement standing tile is a wall.
		 */
		switch (angle) {
		case Directions.UP:
			if (Y < 0)
				return true;
			Y--;
			break;
		case Directions.DOWN:
			if (Y + 1 == worldRenderer.getWorld().getHeight())
				return true;
			Y++;
			break;
		case Directions.LEFT:
			if (X <= 0) {
				X = worldRenderer.getWorld().getWidth() - 1;
				return true;
			}
			X--;
			break;
		case Directions.RIGHT:
			if (X + 1 == worldRenderer.getWorld().getWidth())
				return true;
			X++;
			break;
		}
		return !(worldRenderer.getWorld().getMaze().getElementAt(X, Y) instanceof Wall);

	}

	/**
	 * This function will remove the pellet at the PacMan's position
	 */
	public void removePellet() {
		int X = (int) Math.round(worldRenderer.getWorld().getPacman().getPosition().x);
		int Y = (int) Math.round(worldRenderer.getWorld().getPacman().getPosition().y);
		if (worldRenderer.getWorld().getMaze().getElementAt(X, Y) instanceof Pellet) {
			worldRenderer.getWorld().getMaze().setElementAt(X, Y, new Dark(X, Y));
			hud.addScore(10);
		}
	}

}
