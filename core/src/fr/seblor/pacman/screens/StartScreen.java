package fr.seblor.pacman.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class StartScreen implements Screen {

	BitmapFont _font;
	SpriteBatch _batch;
	Stage _stage;
	
	TextButton _button;

	TextButtonStyle _textButtonStyle;
	TextureAtlas buttonAtlas;

	public StartScreen() {
		super();
		_batch = new SpriteBatch();
		_stage = new Stage();

		_font = new BitmapFont(Gdx.files.internal("font/crackman-128.fnt"));
		_font.setColor(Color.WHITE);

		_textButtonStyle = new TextButtonStyle();
		_textButtonStyle.font = _font;
		_button = new TextButton("Start", _textButtonStyle);

		_button.setWidth(200f);
		_button.setHeight(20f);
		_button.setPosition(Gdx.graphics.getWidth() / 2 - 100f, Gdx.graphics.getHeight() / 2 - 10f);

		_button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("You clicked the button");
			}
		});

		_stage.addActor(_button);

		Gdx.input.setInputProcessor(_stage);
	}

	@Override
	public void show() {

		_batch = new SpriteBatch();

	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

		_batch.begin();
		_stage.draw();
		_batch.end();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		_batch.dispose();
		_font.dispose();
	}

}
