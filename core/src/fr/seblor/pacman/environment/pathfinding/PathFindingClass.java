package fr.seblor.pacman.environment.pathfinding;

import java.util.ArrayList;
import java.util.LinkedList;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class PathFindingClass<ArrayClass, WallClass> {

	int width, height;
	Array<ArrayClass> array;
	ArrayList<Vector2> visited = new ArrayList<Vector2>();

	ArrayList<Node> nodeList = new ArrayList<Node>();
	ArrayList<Node> endNodes = new ArrayList<Node>();

	WallClass wallObject;

	/**
	 * 
	 * @param array
	 *            Your 2D matrix.
	 * @param width
	 *            Width of your 2D matrix.
	 */
	public PathFindingClass(Array<ArrayClass> array, WallClass wall, int width) {
		this.width = width;
		this.height = array.size / width;
		this.array = array;
		this.wallObject = wall;
	}

	/**
	 * 
	 * @param start
	 *            Your starting point
	 * @param ends
	 *            List of your end points
	 * @return An ArrayList of LinkedList with the best path for each of your
	 *         end point
	 */
	public ArrayList<LinkedList<Vector2>> bestPaths(Vector2 start, ArrayList<Vector2> ends) {

		Vector2 correctedVector = new Vector2();

		for (Vector2 end : ends) {
			correctedVector.x = (int) Math.floor(end.x);
			correctedVector.y = (int) Math.floor(end.y);
			endNodes.add(new Node(null, new Vector2(correctedVector)));
		}

		listAllPaths(new Node(null, start));

		ArrayList<LinkedList<Vector2>> pathList = new ArrayList<LinkedList<Vector2>>();
		LinkedList<Vector2> path = new LinkedList<Vector2>();

		for (Node endNode : endNodes) {
			Node currentNode = endNode;
			path.addFirst(endNode.getCoordinates());

			while (currentNode.getPrevious() != null) {
				currentNode = currentNode.getPrevious();
				path.addLast(currentNode.getCoordinates());
			}

			pathList.add(new LinkedList<Vector2>(path));
			path.clear();
		}

		endNodes.clear();
		nodeList.clear();
		visited.clear();

		return pathList;

	}

	public void listAllPaths(Node start) {

		visite(start);

		while (!endsVisited()) {
			ArrayList<Node> ProcessingNodeList = new ArrayList<Node>(nodeList);
			for (Node node : ProcessingNodeList) {
				visite(node);
			}
		}

	}

	public void visite(Node current) {
		for (Vector2 neighbor : getNeighborhood(current.getCoordinates())) {
			ifEndNode(neighbor, current);
			Node newNode = new Node(current, neighbor);
			nodeList.add(newNode);
			visited.add(newNode.getCoordinates());
		}
	}

	public ArrayClass getElementAt(int X, int Y) {
		return array.get(X * width + Y);
	}

	public ArrayClass getElementAt(int index) {
		return array.get(index);
	}

	public ArrayList<Vector2> getNeighborhood(Vector2 element) {

		int X = (int) Math.floor(element.x), Y = (int) Math.floor(element.y);

		ArrayList<Vector2> neighborhood = new ArrayList<Vector2>();
		int height = array.size / width;

		Vector2 coordToTest = new Vector2(X, Y);

		// For each case, check if coordinate to test on border of matrix, check
		// if elements class is path, and if it wasn't visited.

		coordToTest.x = X - 1;
		if ((X > 0) && isPath(coordToTest) && !visited.contains(coordToTest)) {
			neighborhood.add(new Vector2(X - 1, Y));
		}

		coordToTest.x = X + 1;
		if ((X < width - 1) && isPath(coordToTest) && !visited.contains(coordToTest)) {
			neighborhood.add(new Vector2(X + 1, Y));
		}

		coordToTest.x = X;

		coordToTest.y = Y - 1;
		if ((Y > 0) && isPath(coordToTest) && !visited.contains(coordToTest)) {
			neighborhood.add(new Vector2(X, Y - 1));
		}

		coordToTest.y = Y + 1;
		if ((Y < height - 1) && isPath(coordToTest) && !visited.contains(coordToTest)) {
			neighborhood.add(new Vector2(X, Y + 1));
		}

		return neighborhood;

	}

	public boolean endsVisited() {
		boolean allVisited = true;
		for (Node node : endNodes) {
			if (node.getPrevious() == null)
				allVisited = false;
		}
		return allVisited;
	}

	public void ifEndNode(Vector2 neighbor, Node current) {
		for (Node endNode : endNodes) {
			if (neighbor.equals(endNode.getCoordinates()))
				endNode.setPrevious(current);
		}
	}

	public boolean isPath(Vector2 element) {
		return !(getElementAt((int) Math.floor(element.x), (int) Math.floor(element.y)).getClass()
				.equals(wallObject.getClass()));
	}

}
