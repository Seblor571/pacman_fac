package fr.seblor.pacman.environment.pathfinding;

import com.badlogic.gdx.math.Vector2;

public class Node {

	private Node previousNode = null;
	private Vector2 coordinates = null;

	public Node(Node previous, Vector2 coordinates) {
		this.previousNode = previous;
		this.coordinates = coordinates;
	}

	public Node getPrevious() {
		return previousNode;
	}

	public void setPrevious(Node previous) {
		this.previousNode = previous;
	}

	public Vector2 getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Vector2 coordinates) {
		this.coordinates = coordinates;
	}

	@Override
	public String toString() {
		return "Node [coordinates=" + coordinates + "]";
	}

}
