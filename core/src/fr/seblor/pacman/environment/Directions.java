package fr.seblor.pacman.environment;

import com.badlogic.gdx.math.Vector2;

public class Directions {
	public static final int RIGHT = 0;
	public static final int UP = 90;
	public static final int LEFT = 180;
	public static final int DOWN = 270;

	public static boolean isHorizontal(int angle) {
		return (angle % 180 == 0);
	}

	public static boolean isVertical(int angle) {
		return !isHorizontal(angle);
	}

	public static boolean isSameWay(int angle1, int angle2) {
		return ((angle1 + angle2) % 180 == 0);
	}

	public static boolean areOpposites(int angle1, int angle2) {
		return (isSameWay(angle1, angle2) && (angle1 != angle2));
	}

	public static int getDirFromCoords(Vector2 v1, Vector2 v2) {
		if ((v1.x == v2.x - 1) && (v1.y == v2.y))
			return Directions.RIGHT;
		else if ((v1.x == v2.x + 1) && (v1.y == v2.y))
			return Directions.LEFT;
		else if ((v1.x == v2.x) && (v1.y == v2.y - 1))
			return Directions.DOWN;
		else
			return Directions.UP;
	}
}
