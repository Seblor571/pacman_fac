package fr.seblor.pacman.structures;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import fr.seblor.pacman.entities.Bloc;
import fr.seblor.pacman.entities.Dark;
import fr.seblor.pacman.entities.GameElement;
import fr.seblor.pacman.entities.Ghost;
import fr.seblor.pacman.entities.PacMan;
import fr.seblor.pacman.entities.Pellet;
import fr.seblor.pacman.entities.SuperPellet;
import fr.seblor.pacman.entities.Wall;
import fr.seblor.pacman.entities.ghosts.Blinky;
import fr.seblor.pacman.entities.ghosts.Clyde;
import fr.seblor.pacman.entities.ghosts.Inky;
import fr.seblor.pacman.entities.ghosts.Pinky;
import fr.seblor.pacman.environment.pathfinding.PathFindingClass;

public class World implements Iterable<GameElement> {

	Maze maze;

	private PathFindingClass<Bloc, Wall> pathFinder;

	private ArrayList<Vector2> ghostHouse;

	int nbElements = 0;

	private PacMan pacman;
	private Array<Ghost> ghosts = new Array<Ghost>();

	public PathFindingClass<Bloc, Wall> getPathFinder() {
		return pathFinder;
	}

	public ArrayList<Vector2> getGhostHouse() {
		return ghostHouse;
	}

	public Maze getMaze() {
		return maze;
	}

	public int getWidth() {
		if (maze == null)
			return 0;
		return maze.getWidth();
	}

	public int getHeight() {
		if (maze == null)
			return 0;
		return maze.getHeight();
	}

	public boolean isGhostAt(int index) {

		int X = (index - 1) / getWidth();
		int Y = (index - 1) % getWidth();

		for (Ghost ghost : ghosts) {
			if (ghost.getPosition().x == X && ghost.getPosition().y == Y)
				return true;
		}

		return false;
	}

	public boolean isGhostAt(int X, int Y) {
		return isGhostAt(X * getWidth() + Y);

	}

	public boolean initialize(String filename) {
		FileHandle file = Gdx.files.internal(filename);

		if (!file.exists())
			return false;

		try {

			ghostHouse = new ArrayList<Vector2>();

			String map = file.readString();

			int height = map.split(System.getProperty("line.separator")).length;
			int width = map.split(System.getProperty("line.separator"))[0].split(",").length;

			maze = new Maze(width, height);

			String lines[] = map.split(System.getProperty("line.separator"));
			String[] entities;

			String entity;

			for (int Y = 0; Y < lines.length; Y++) {

				entities = lines[Y].split(",");

				for (int X = 0; X < entities.length; X++) {

					entity = entities[X];

					short entityType = Short.parseShort(entity.trim().split(":")[0]);
					short metadata = Short.parseShort(entity.trim().split(":")[1]);

					System.out.println("Entity type : " + entityType + " | metadata : " + metadata);

					switch (entityType) {
					case 0:
						maze.setElementAt(X, Y, new Wall(X, Y));
						break;
					case 1:
						this.pacman = new PacMan(0, X, Y);
						maze.setElementAt(X, Y, new Dark(X, Y));
						break;
					case 2:
						ghostHouse.add(new Vector2(X, Y));
						switch (metadata) {
						case 1:
							this.ghosts.add(new Inky(X, Y, maze.getStructMap()));
							break;
						case 2:
							this.ghosts.add(new Blinky(X, Y, maze.getStructMap()));
							break;
						case 3:
							this.ghosts.add(new Pinky(X, Y, maze.getStructMap()));
							break;
						case 4:
							this.ghosts.add(new Clyde(X, Y, maze.getStructMap()));
							break;
						default:
							break;
						}
						maze.setElementAt(X, Y, new Dark(X, Y));
						break;

					default:
						if (metadata == 1)
							maze.setElementAt(X, Y, new SuperPellet(X, Y));
						else
							maze.setElementAt(X, Y, new Pellet(X, Y));
						break;
					}

					System.out.println("X: " + X + " | Y: " + Y);

					nbElements++;
				}
			}

			// 4 ghosts + 1 pacman = 5
			nbElements += 5;

			System.out.println(nbElements);

			this.pathFinder = new PathFindingClass<Bloc, Wall>(maze.getStructMap(), new Wall(0, 0), width);

			return true;

		} catch (Exception e) {

			return false;
		}
	}

	public PacMan getPacman() {
		return this.pacman;
	}

	public Array<Ghost> getGhosts() {
		return ghosts;
	}

	public void setGhosts(Array<Ghost> ghosts) {
		this.ghosts = ghosts;
	}

	@Override
	public Iterator<GameElement> iterator() {

		Iterator<GameElement> it = new Iterator<GameElement>() {

			private int currentIndex = 0;

			@Override
			public boolean hasNext() {
				// walls/pellets > ghosts > Pacman
				return (currentIndex < nbElements);

			}

			@Override
			public GameElement next() {

				// walls/pellets > ghosts > Pacman

				int nbStructs = getWidth() * getHeight();

				if (currentIndex < nbStructs)
					return maze.getElementAt(currentIndex++);

				if (currentIndex != nbElements - 1) {
					currentIndex++;
					return ghosts.get(nbElements - currentIndex - 1);
				}

				currentIndex++;
				return pacman;

			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
		return it;
	}

}
