package fr.seblor.pacman.structures;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;

import fr.seblor.pacman.entities.Bloc;
import fr.seblor.pacman.entities.GameElement;
import fr.seblor.pacman.entities.Wall;

public class Maze implements Iterable<GameElement> {

	private int width;
	private int height;

	String map;

	private Array<Bloc> structMap = new Array<Bloc>();

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Maze(int width, int height) {

		this.width = width;
		this.height = height;

		for (int i = 0; i < width * height; i++) {
			structMap.insert(i, null);
		}

		System.out.println("Creating map. Height=" + height + ", Width=" + width);

	}

	public Bloc getElementAt(int X, int Y) {
		try {

			return structMap.get(X * width + Y);
		} catch (Exception e) {
			return new Wall(0, 0);
		}
	}

	public Array<Bloc> getStructMap() {
		return structMap;
	}

	public Bloc getElementAt(int index) {
		return structMap.get(index);
	}

	public void setElementAt(int X, int Y, Bloc entity) {

		// System.out.printf("X: %d | Y: %d\n", X, Y);

		structMap.set(X * width + Y, entity);
	}

	@Override
	public Iterator<GameElement> iterator() {

		Iterator<GameElement> it = new Iterator<GameElement>() {

			private int currentIndex = 0;

			@Override
			public boolean hasNext() {

				return currentIndex < width * height;

			}

			@Override
			public GameElement next() {

				return getElementAt(currentIndex++);

			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
		return it;
	}

}
