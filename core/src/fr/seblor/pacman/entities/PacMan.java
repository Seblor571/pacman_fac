package fr.seblor.pacman.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

import fr.seblor.pacman.TextureFactory;

public class PacMan extends GameElement {
	private Vector2 initialPosition;
	private int angle;

	private boolean alive;
	private int nextAngle;
	private int animationState = 3;

	/**
	 * Speed value = 1/speed of block size
	 */
	public static final float speed = 1f / 10f;

	public PacMan(int angle, int X, int Y) {
		super();
		this.angle = angle;
		this.alive = true;
		this.initialPosition = new Vector2(X, Y);
		this.position = new Vector2(X, Y);
	}

	public void updateAngle() {
		this.angle = this.nextAngle;
	}

	public int getAngle() {
		return angle;
	}

	public int getNextAngle() {
		return nextAngle;
	}

	public void setNextAngle(int nextAngle) {
		this.nextAngle = nextAngle;
	}

	public boolean isAlive() {
		return alive;
	}

	@Override
	public Texture getTexture() {

		return TextureFactory.getInstance().getTexture(this.getClass());
		// return TextureFactory.getInstance().getTexturePacMan(this);
	}

	@Override
	public int getWidth() {
		return 16;
	}

	@Override
	public int getHeight() {
		return 16;
	}

	public Vector2 getInitialPosition() {
		return initialPosition;
	}

	public int getAnimationState() {
		/*
		 * Change the animation state every 4 ticks
		 */
		return (animationState = (animationState + 1) % 12) / 4 + 1;
	}

}
