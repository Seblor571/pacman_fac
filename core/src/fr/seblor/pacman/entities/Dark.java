package fr.seblor.pacman.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

import fr.seblor.pacman.TextureFactory;

public class Dark extends Bloc {
	
	public Dark(int X, int Y) {
		this.position = new Vector2(X, Y);
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return 16;
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 16;
	}

	@Override
	public Texture getTexture() {
		return TextureFactory.getInstance().getTexture(this.getClass());
		// return TextureFactory.getInstance().getTextureDark();
	}

}
