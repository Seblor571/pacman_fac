package fr.seblor.pacman.entities.ghosts;

import com.badlogic.gdx.utils.Array;

import fr.seblor.pacman.entities.Bloc;
import fr.seblor.pacman.entities.Ghost;

public class Clyde extends Ghost {

	public Clyde(int X, int Y, Array<Bloc> array) {
		super(X, Y, array);
	}

}
