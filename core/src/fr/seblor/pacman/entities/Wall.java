package fr.seblor.pacman.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

import fr.seblor.pacman.TextureFactory;

public class Wall extends Bloc {

	public Wall(int X, int Y) {
		this.position = new Vector2(X, Y);
	}

	@Override
	public int getWidth() {
		return 64;
	}

	@Override
	public int getHeight() {
		return 64;
	}

	@Override
	public Texture getTexture() {
		return TextureFactory.getInstance().getTexture(this.getClass());
		// return TextureFactory.getInstance().getTextureWall();
	}

}
