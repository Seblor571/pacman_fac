package fr.seblor.pacman.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

public abstract class GameElement {

	Vector2 position;
	float size;

	public Vector2 getPosition() {
		return position;
	}

	public float getSize() {
		return size;
	}

	public abstract int getWidth();
	public abstract int getHeight();
	
	public abstract Texture getTexture();

	public void setPosition(Vector2 position) {
		this.position = position;
	}

}
