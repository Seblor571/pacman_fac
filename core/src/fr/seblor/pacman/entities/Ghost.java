package fr.seblor.pacman.entities;

import java.util.LinkedList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import fr.seblor.pacman.TextureFactory;

public class Ghost extends GameElement {

	private boolean alive;
	private boolean escaping;
	private Vector2 initialPosition;
	private int angle;
	private boolean followPath = false;
	private LinkedList<Vector2> path = new LinkedList<Vector2>();
	
	/**
	 * Speed value = 1/speed of block size
	 * 
	 * @return
	 */
	public float getSpeed() {
		return 1f / ((escaping) ? (12f) : (7f));
	}

	public Ghost(int X, int Y, Array<Bloc> array) {
		super();
		this.alive = true;
		this.escaping = false;
		this.initialPosition = new Vector2(X, Y);
		this.position = new Vector2(X, Y);
	}

	public boolean isAlive() {
		return alive;
	}

	public boolean isEscaping() {
		return escaping;
	}

	@Override
	public Texture getTexture() {
		return TextureFactory.getInstance().getTexture(this.getClass());
		// return TextureFactory.getInstance().getTextureGhost(this);
	}

	@Override
	public int getWidth() {
		return 16;
	}

	@Override
	public int getHeight() {
		return 16;
	}

	public Vector2 getInitialPosition() {
		return initialPosition;
	}

	@Override
	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public int getAngle() {
		return angle;
	}

	public void setAngle(int angle) {
		if (this.angle != angle)
			this.followPath = !this.followPath;
		this.angle = angle;
	}
	
	public boolean isFollowingPath() {
		return this.followPath;
	}

	public LinkedList<Vector2> getPath() {
		return path;
	}

	public void setPath(LinkedList<Vector2> path) {
		this.path.clear();
		this.path.addAll(path);
	}

}
