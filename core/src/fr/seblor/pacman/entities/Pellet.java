package fr.seblor.pacman.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

import fr.seblor.pacman.TextureFactory;

public class Pellet extends Bloc {

	private boolean alive;

	public Pellet(int X, int Y) {
		this.alive = true;

		this.position = new Vector2(X, Y);
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	@Override
	public Texture getTexture() {
		return TextureFactory.getInstance().getTexture(this.getClass());
		// return TextureFactory.getInstance().getTexturePellet(this);
	}

	@Override
	public int getWidth() {
		return 16;
	}

	@Override
	public int getHeight() {
		return 16;
	}

}