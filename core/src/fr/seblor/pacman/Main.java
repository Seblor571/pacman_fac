package fr.seblor.pacman;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import fr.seblor.pacman.screens.PlayingScreen;
import fr.seblor.pacman.screens.StartScreen;

public class Main extends Game {
	SpriteBatch batch;
	Texture img;

	@Override
	public void create() {

		setScreen(new PlayingScreen());
		//setScreen(new StartScreen());
		
	}

	@Override
	public void render() {
		
		super.render();
		
	}
}

