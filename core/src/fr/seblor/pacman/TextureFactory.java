package fr.seblor.pacman;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import fr.seblor.pacman.entities.Dark;
import fr.seblor.pacman.entities.PacMan;
import fr.seblor.pacman.entities.Pellet;
import fr.seblor.pacman.entities.SuperPellet;
import fr.seblor.pacman.entities.Wall;
import fr.seblor.pacman.entities.ghosts.Blinky;
import fr.seblor.pacman.entities.ghosts.Clyde;
import fr.seblor.pacman.entities.ghosts.Inky;
import fr.seblor.pacman.entities.ghosts.Pinky;
import fr.seblor.pacman.environment.Directions;

public class TextureFactory {

	// private Hashtable<String, Texture> textureMap = new Hashtable<String,
	// Texture>();
	private HashMap<Class<?>, Texture> textureList;
	private HashMap<String, Texture> pacmanTextures;
	private int currentIndexPM = 1;
	private int buffer = 0; // prevent PacMan to change texture each iteration

	private TextureFactory() {
		/*
		 * FileHandle[] textures = Gdx.files.internal("textures").list(); for
		 * (FileHandle fileHandle : textures) {
		 * textureMap.put(fileHandle.name(), new
		 * Texture(Gdx.files.internal("textures/" + fileHandle.name()))); }
		 */

		textureList = new HashMap<Class<?>, Texture>();
		pacmanTextures = new HashMap<String, Texture>();

		textureList.put(Dark.class, new Texture(Gdx.files.internal("textures/dark.png")));
		textureList.put(Inky.class, new Texture(Gdx.files.internal("textures/ghost1.png")));
		textureList.put(Blinky.class, new Texture(Gdx.files.internal("textures/ghost2.png")));
		textureList.put(Pinky.class, new Texture(Gdx.files.internal("textures/ghost3.png")));
		textureList.put(Clyde.class, new Texture(Gdx.files.internal("textures/ghost4.png")));
		textureList.put(PacMan.class, new Texture(Gdx.files.internal("textures/pacmanRight.png")));
		textureList.put(Pellet.class, new Texture(Gdx.files.internal("textures/pellet.png")));
		textureList.put(SuperPellet.class, new Texture(Gdx.files.internal("textures/superpellet-2.png")));
		textureList.put(Wall.class, new Texture(Gdx.files.internal("textures/bloc.png")));

		pacmanTextures.put("right1", new Texture(Gdx.files.internal("textures/pacmanRight.png")));
		pacmanTextures.put("right2", new Texture(Gdx.files.internal("textures/pacmanRight-2.png")));
		pacmanTextures.put("up1", new Texture(Gdx.files.internal("textures/pacmanUp.png")));
		pacmanTextures.put("up2", new Texture(Gdx.files.internal("textures/pacmanUp-2.png")));
		pacmanTextures.put("left1", new Texture(Gdx.files.internal("textures/pacmanLeft.png")));
		pacmanTextures.put("left2", new Texture(Gdx.files.internal("textures/pacmanLeft-2.png")));
		pacmanTextures.put("down1", new Texture(Gdx.files.internal("textures/pacmanDown.png")));
		pacmanTextures.put("down2", new Texture(Gdx.files.internal("textures/pacmanDown-2.png")));
		pacmanTextures.put("default", new Texture(Gdx.files.internal("textures/pacman-3.png")));

	}

	private static TextureFactory textureFactory = new TextureFactory();

	public static TextureFactory getInstance() {
		return textureFactory;
	}

	public Texture getTexture(Class<?> c) {
		return this.textureList.get(c);
	}

	public void setPacManTexture(int direction) {

		if (++buffer != 5) {
			return;
		}
		buffer = 0;

		currentIndexPM++;
		if (currentIndexPM == 3) {
			textureList.put(PacMan.class, pacmanTextures.get("default"));
			currentIndexPM = 0;
			return;
		}
		switch (direction) {
		case Directions.RIGHT:
			textureList.put(PacMan.class, pacmanTextures.get("right" + currentIndexPM));
			break;
		case Directions.UP:
			textureList.put(PacMan.class, pacmanTextures.get("up" + currentIndexPM));
			break;
		case Directions.LEFT:
			textureList.put(PacMan.class, pacmanTextures.get("left" + currentIndexPM));
			break;
		case Directions.DOWN:
			textureList.put(PacMan.class, pacmanTextures.get("down" + currentIndexPM));
			break;

		default:
			break;
		}
	}
}
