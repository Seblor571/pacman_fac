package fr.seblor.pacman.gui;

import java.util.TimerTask;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Hud {

	private BitmapFont _debugFont = new BitmapFont();

	private java.util.Timer _timer;
	private Vector2 _timerPos;

	/*
	 * Score variables
	 */
	private int _score;
	private String _strScore;
	private Vector2 _scorePos;

	public Hud() {

		_score = -1;

		_timer = null;
	}

	public void addScoreElement(int X, int Y) {
		setScorePosition(X,Y);
		_score = 0;
		scoreToString();
	}
	public void setScorePosition(int X, int Y) {
		_scorePos = new Vector2(X, Y);
	}

	public void removeScoreElement() {
		_scorePos = null;
		_score = -1;
	}

	public void addTimerElement(int X, int Y) {
		setTimerPosition(X,Y);
		_timer = new java.util.Timer();
		_timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				fr.seblor.pacman.gui.Timer.getInstance().addTime(1);
			}
		}, 0, 1000);
	}

	public void setTimerPosition(int X, int Y) {
		_timerPos = new Vector2(X, Y);
	}

	public void removeTimerElement() {
		_timer.cancel();
		_timer = null;
	}

	public int getScore() {
		return _score;
	}

	public void setScore(int score) {
		this._score = score;
		scoreToString();
	}

	public void resetScore() {
		_score = 0;
		scoreToString();
	}

	public void addScore(int addValue) {
		_score += addValue;
		scoreToString();
	}

	public void drawHUD(SpriteBatch batch) {
		if (_score != -1)
			_debugFont.draw(batch, _strScore, _scorePos.x, _scorePos.y);
		if (_timer != null)
			_debugFont.draw(batch, Timer.getInstance().toString(), _timerPos.x, _timerPos.y);
	}

	private void scoreToString() {
		_strScore = Integer.toString(_score);
	}

}
