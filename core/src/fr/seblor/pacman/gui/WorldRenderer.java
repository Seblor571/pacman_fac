package fr.seblor.pacman.gui;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import fr.seblor.pacman.entities.GameElement;
import fr.seblor.pacman.structures.World;

public class WorldRenderer {

	private OrthographicCamera _camera;
	private World world;

	private int squareSize;

	public int getSquareSize() {
		return squareSize;
	}
	
	public World getWorld() {
		return world;
	}

	public WorldRenderer() {

		/*
		 * setting up the camera
		 */
		_camera = new OrthographicCamera();
	}

	public OrthographicCamera getCamera() {
		return _camera;
	}

	public boolean initialize(String fileName) {

		this.world = new World();

		return world.initialize(fileName);
	}

	/*
	 * Functions called by the Screen
	 */
	public void render(float delta, SpriteBatch batch) {

		for (GameElement gameElement : world) {

			if (gameElement != null) {
				// drawTeleport(gameElement);
				drawSmooth(gameElement, batch);
			} else {
				System.out.println("null");
			}
		}
	}

	public void resize(int width, int height) {

		if ((world.getWidth() != 0) && (world.getHeight() != 0))
			squareSize = ((width - height < 0)) ? (width / world.getWidth()) : (height / world.getHeight());

		int min = Math.min(width, height);

		_camera.setToOrtho(false);
		_camera.position.set(min / 2, min / 2, 0);
		_camera.update();
	}

	/*
	 * Two ways of displaying the animations
	 */
	private void drawTeleport(GameElement gameElement, SpriteBatch batch) {
		batch.draw(gameElement.getTexture(), (int) Math.floor(gameElement.getPosition().x) * squareSize,
				(world.getMaze().getHeight() - (int) Math.floor(gameElement.getPosition().y) - 1) * squareSize,
				squareSize, squareSize);
	}

	private void drawSmooth(GameElement gameElement, SpriteBatch batch) {
		batch.draw(gameElement.getTexture(), gameElement.getPosition().x * squareSize,
				(world.getMaze().getHeight() - gameElement.getPosition().y - 1) * squareSize, squareSize, squareSize);
	}

}
