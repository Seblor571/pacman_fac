package fr.seblor.pacman.gui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Timer {

	private static Timer _timer = new Timer();

	private int _time = 0;

	public static Timer getInstance() {
		return _timer;
	}

	public static int getTimerWidth() {
		BitmapFont font = new BitmapFont();
		int width = (int) Math.round((5 * font.getXHeight()) + 2 * font.getSpaceWidth());
		font.dispose();
		return width;
	}

	public void restart() {
		_time = 0;
	}

	public void setTime(int _time) {
		this._time = _time;
	}

	public int getTimeInSec() {
		return _time;
	}

	public float getTimeInMin() {
		return ((float) _time) / 60f;
	}

	public int getFullMins() {
		return _time / 60;
	}

	public void addTime(int seconds) {
		_time += seconds;
	}

	/**
	 * Return a string formated as MM:SS
	 */
	public String toString() {
		return String.format("%02d : %02d", getFullMins(), _time % 60);
	}

	public String toString(String separator) {
		return String.format("%02d " + separator + " %02d", getFullMins(), _time % 60);
	}

}
